@extends('layouts.app')

@section('content')
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">                
            <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('error'))
        <div class="alert alert-danger" role="alert">                
            <strong>{{ $message }}</strong>
            </div>
            @endif
            @if (Auth::user()->tipo_usuario==0)
            <div class="card">
                <div class="card-header">{{ __('Bienvenido') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Ingresa la siguiente informacion!') }}
                    <br>
                    <form method="POST" action="{{ route('home')}}">
                        @csrf
                        <div class="form-group">
                            <label for="hrs">Hora de entrada</label>
                            <input type="time" class="form-control" name="hora" required >
                        </div>
                        <div class="form-group">
                            <label for="hrs">Comentarios</label>
                            <input type="text" class="form-control" name="comentarios" >
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
                
                
                
            </div>
            @endif
            @if (Auth::user()->tipo_usuario==1)
            <h2>Lista de Usuarios</h2>

            <table>
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Hora de entrada</th>
                <th>Nota</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($chequeo as $chequeos) 
            <tr>
                <td>{{$chequeos->id}}</td>
                <td>{{$chequeos->user->name}}</td>
                <td>{{$chequeos->hora}}</td>
                
                @if ($chequeos->hora >= ("09:41:00"))
                    <td style="color:red;">Retardo</td>
                
                @else
                    <td style="color:green;">En tiempo</td>
                
                @endif
            </tr>
            @endforeach
                    </tbody>
            </table>
        </div>
        @endif
    </div>
</div>
@endsection
