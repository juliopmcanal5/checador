<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Check;
use App\Models\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $chequeo = Check::with('user')->get();
        return view('home',compact('chequeo'));
    }

    public function store(Request $request)
    {
        $user = $request->user()->id;
        $hoy = Carbon::today();
        $chequeo=Check::orderBy('created_at','DESC')
        ->whereDate('created_at', '=', $hoy)
        ->where('user_id',$user)
        ->first(); 
        if($chequeo){
            return redirect()->route('home')->with('error','Ya registraste la hora de entrada');
        }
        else    
        $chequeo = new Check ([
            'hora' => $request->get('hora'),
            'comentarios' => $request->get('comentarios'),
            'user_id'=>$request->user()->id
        ]);
        $chequeo->save();

        return redirect()->route('home')->with('success','El chequeo se realizo con exitosamente!');
    }
}
