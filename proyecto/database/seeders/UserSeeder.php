<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' =>  'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'tipo_usuario' => '1'

        ]);

        User::create([
            'name' =>  'usuario1',
            'email' => 'usuario1@gmail.com',
            'password' => bcrypt('usuario1'),
            'tipo_usuario' => '0'

        ]);

    }
}
